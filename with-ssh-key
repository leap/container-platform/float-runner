#!/bin/sh
#
# Wrap a command with an ssh-agent with loaded credentials.
# The (optional) private key is passed as the environment variable
# SSH_PRIVATE_KEY.
#
# This tool will also generate a .ssh/config file based on other
# environment variables that might be present:
#
# SSH_PORT_MAPPINGS: a comma-separated list of "host:port" pairs,
# telling ssh to use a non-standard port for that specific host.
#
# SSH_KNOWN_HOSTS: contents of ~/.ssh/known_hosts.
#

key=${SSH_PRIVATE_KEY:-}
if [ -z "$key" ]; then
    echo "no SSH_PRIVATE_KEY variable found"
    "$@"
    exit $?
fi

mkdir -p ~/.ssh
chmod 0700 ~/.ssh
cat << EOF > ~/.ssh/config
Host remotevirt.riseup.net
   Port 4422
   StrictHostKeyChecking no
EOF
echo "$key" > ~/.ssh/key
chmod 0600 ~/.ssh/key
eval `ssh-agent -s`
trap "ssh-agent -k >/dev/null" EXIT

ssh-add ~/.ssh/key
if [ $? -gt 0 ]; then
    echo "ERROR: could not load SSH_PRIVATE_KEY" >&2
    exit 2
fi

if [ -n "${SSH_PORT_MAPPINGS:-}" ]; then
    : > ~/.ssh/config
    for mapping in $(echo "$SSH_PORT_MAPPINGS" | tr , ' '); do
        host=${mapping%%:*}
        port=${mapping##*:}
        echo "Host ${host}" >> ~/.ssh/config
        echo "    Port ${port}" >> ~/.ssh/config
    done
fi

if [ -n "${SSH_KNOWN_HOSTS:-}" ]; then
    echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
fi

"$@"
exit $?

