#!/bin/sh
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="
	golang
"

# Packages required to serve the website and run the services.
PACKAGES="
	bind9
	bind9utils
	build-essential
	libsodium23
	curl
	git
	openssh-client
	ansible
	vagrant
	vagrant-libvirt
	libvirt-clients
	python3-jinja2
	python3-six
	python3-yaml
	python3-pysodium
	python3-virtualenv
"

MITOGEN_VERSION="0.3.4"
MITOGEN_URL="https://files.pythonhosted.org/packages/source/m/mitogen/mitogen-${MITOGEN_VERSION}.tar.gz"

GO_PKGS="
	git.autistici.org/ale/x509ca@latest
	git.autistici.org/ale/ed25519gen@latest
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e
umask 022

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Download and install Mitogen.
(cd /usr/src ; \
   curl -sL ${MITOGEN_URL} | tar xzf - ; \
    chown -R root.root mitogen-${MITOGEN_VERSION})
echo "export MITOGEN=/usr/src/mitogen-${MITOGEN_VERSION}" \
      > /etc/profile.d/mitogen.sh

# Install Go packages.
for pkg in $GO_PKGS; do
    go install ${pkg}
done
mv $HOME/go/bin/* /usr/bin/ 

# Set up investici.org SSH CA.
echo "@cert-authority *.float.bitmask.net.org ssh-ed25519 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBb7nribVWxCd8Jx/DBUu3RSyTsIpf84J3oENr825oga" \
    > /etc/ssh/ssh_known_hosts

# Ensure our tools are executable.
chmod 755 /usr/bin/with-ssh-key
chmod 755 /usr/bin/floatup.py
ln -s floatup.py /usr/bin/floatup

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
