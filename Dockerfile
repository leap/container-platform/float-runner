FROM debian:12-slim
COPY build.sh /tmp/build.sh
COPY with-ssh-key /usr/bin/with-ssh-key
COPY floatup.py /usr/bin/floatup.py
RUN /tmp/build.sh && rm /tmp/build.sh

